﻿using System;
using System.Collections;

namespace Pendu
{
    class Program
    {
        
        static string GenerateWord()
        {
            string[] List = {"CONFIGURER" ,
                    "AMELANCHIER" ,
                    "ACAJOU" ,
                    "CERCLE" ,
                    "OUTILS" ,
                    "CAIMAN" ,
                    "BURINE" ,
                    "CHLEUASME" ,
                    "BASSON" ,
                    "AZIMUT" ,
                    "ALPHES" ,
                    "MENTHE" ,
                    "WHISKY" ,
                    "GOURGANDINE" ,
                    "ACCES" ,
                    "CLIP" ,
                    "RHUM" ,
                    "CORSE" ,
                    "SKI" ,
                    "SIX" ,
                    "FAC" ,
                    "ADO" ,
                    "GANG" ,
                    "NYCTALOPE" ,
                    "SCHOINOPENTAXOPHILE" ,
                    "SUPERFETATOIRE" ,
                    "MANGER" ,
                    "DORMIR" +
                    "UNIQUE" +
                    "RAMONER"};
            Random Rand = new Random();
            int choose = Rand.Next(0, List.Length);
            string Word = List[choose];
            return Word;
        }

        static string GenerateCountry()
        {
            string[] List =
            {
                "FRANCE",
                "ALLEMAGNE",
                "ITALIE",
                "BELGIQUE",
                "CHINE",
                "UKRANIE",
                "RUSSIE",
                "ALGERIE",
                "SUISSE",
                "ESPAGNE",
                "POLOGNE",
                "PEROU",
                "CANADA",
                "INDE",
                "IRLANDE"
            };
            Random Rand = new Random();
            int choose = Rand.Next(0, List.Length);
            string Word = List[choose];
            return Word;
        }
        static bool game()
        {
            return true;
        }
        
        static void GameDisplay(int Vie, char[] DecomWord)
        {
            if(Vie > 1)
            {
                Console.WriteLine("Il te reste " + Vie + " Vies");
            }
            else if (Vie == 1)
            {
                Console.WriteLine("Il te reste " + Vie + " Vie");
            }
            Console.WriteLine(" ");
            Console.WriteLine("  +---+  ");
            Console.WriteLine("  |   |  ");
            if (Vie < 6)
            {
                Console.WriteLine("  O   |  ");
                if (Vie < 5)
                {
                    if (Vie < 2)
                    {
                        if (Vie < 1)
                        {
                            Console.WriteLine(" /|\\  |  ");
                            
                        }
                        else
                        {
                            Console.WriteLine(" /|   |  ");
                        }
                    }
                    else
                    {
                        Console.WriteLine("  |   |  ");
                    }
                    if (Vie < 4)
                    {
                        
                        if (Vie < 3)
                        {
                            Console.WriteLine(" / \\  |  ");
                        }
                        else
                        {
                            Console.WriteLine(" /    |  ");
                        }
                    }
                    else
                    {
                        Console.WriteLine("      |  ");
                    }
                }
                else
                {
                    Console.WriteLine("      |  ");
                    Console.WriteLine("      |  ");
                }
            }
            else
            {
                Console.WriteLine("      |  ");
                Console.WriteLine("      |  ");
                Console.WriteLine("      |  ");
            }
            Console.WriteLine("      |  ");
            Console.WriteLine("=========");
            Console.WriteLine(" ");

        }
        static void Main(string[] args)
        {
            Start();
        }
        static void Start()
        {
            int Vie = 6;
            string Word = "";
            int Difficulty = Setup();
            if (Difficulty == 1)
            {
                Word = GenerateCountry();
            }
            else if (Difficulty == 2)
            {
                Word = GenerateWord();
            }
            Char[] DecompWord = Word.ToCharArray();
            int WordLength = Word.Length;
       //     while (game()) ;
            Console.WriteLine("Vies restantes : " + Vie);
            WordDis(WordLength, DecompWord, Word, Vie);
        }

        static int Setup()
        {
            Console.WriteLine("Bienvenue au jeu du Pendu !");
            Console.WriteLine("");
            Console.WriteLine("Choisis le type de mot qut tu veut");
            Console.WriteLine("");
            Console.WriteLine("1 - Pays");
            Console.WriteLine("2 - Mots");
            Console.WriteLine("");
            Console.Write(": ");
            try
            {
                int Diff = int.Parse(Console.ReadLine());
                if (Diff == 1 || Diff == 2)
                {
                    return Diff;
                }
                else
                {
                    Console.WriteLine("tu ne peut pas mettre un nombre différent de 1 ou 2 !");
                    Console.ReadKey();
                    Start();
                }
            }
            catch
            {
                Console.WriteLine("Tu ne peut pas mettre aurte chose que des nombres !");
                Console.WriteLine("Appuie sur une touche pour continuer");
                Console.ReadKey();
                Console.Clear();
                return Setup();
            }
            return 0;
        }

        static void Death(char[] DecomWord)
        {
            Console.WriteLine("");
            Console.WriteLine("Tu n'a plus de vie, tu a perdu...");
            Console.WriteLine("");
            Console.Write("Le mot a deviner étais :");
            for (int i = 0; i<DecomWord.Length; i++)
            {
                Console.Write(DecomWord[i]);
            }
            Console.WriteLine("");
        }

        static void WordDis(int WordLength, char[] DecomWord, string Word, int Vie)
        {
            
            bool LetterInWord = false;
            int i = 0;
            int[] RightLetter = new int[WordLength + 1];

            Play(LetterInWord, RightLetter, DecomWord, WordLength, i, Vie);
            


        }
        static void Play(bool LetterInWord, int[] RightLetter,char[] DecomWord,int WordLength, int i, int Vie)
        {
            char Letter;
            Stack WrongLetter = new Stack();
            bool win = false; 
            while(Vie != 0 && !win)
            {
                Console.Clear();
                Console.WriteLine();
                Console.Write("Tu a déjà essayé les lettres : ");
                foreach (char elem in WrongLetter)
                {
                    Console.Write(elem + ", ");
                }
                
                GameDisplay(Vie, DecomWord);
                i = 0;
                LetterInWord = false;
                DispWord(WordLength, RightLetter, DecomWord, i);
                Console.WriteLine("");
                Console.WriteLine("");
                Console.Write(": ");
                Letter = GetLetter(RightLetter, DecomWord, WrongLetter);
                i = 0;
                while (i != WordLength)
                {

                    if (i < DecomWord.Length && Letter == DecomWord[i])
                    {
                        LetterInWord = true;
                        RightLetter[i] = 1;
                    }
                    
                    i++;
                }
                if (LetterInWord == false)
                {
                    Vie--;
                    WrongLetter.Push(Letter);
                }
                win = TestWin(RightLetter, DecomWord);
            }
            Console.Clear();
            if (Vie == 0)
            {
                GameDisplay(Vie, DecomWord);
                Death(DecomWord);
            }
            else
            {
                Console.WriteLine("g gagné");
            }
        }
        static bool TestWin(int[] RightLetter, char[] DecomWord)
        {
            int NumRightLetter = 0; 
            bool win = false; 
            for (int e = 0; e < RightLetter.Length; e++)
            {
                if (RightLetter[e] == 1)
                {
                    NumRightLetter++;
                }
            }
            if (NumRightLetter == DecomWord.Length)
            {
                win = true; 
            }
            return win;
        }
        static void DispWord(int WordLength, int[] RightLetter, char[] DecomWord, int i)
        {
            while (i != WordLength)
            {
                if (RightLetter[i] == 1)
                {
                    Console.Write(DecomWord[i] + " ");
                }
                else
                {
                    Console.Write("_ ");
                }
                i++;
            }
        }
        static char GetLetter(int[] RightLetter, char[] DecomWord, Stack WrongLetter)
        {
            Char Letter;
            string input;
            try
            {
                input = (Console.ReadLine());
                //Letter = Char.Parse(Console.ReadLine());
                if (input.Length == 1)
                {
                    Letter = Char.ToUpper(input[0]);
                    if(Letter >='A' && Letter <= 'Z')
                    {
                        if(WrongLetter.Contains(Letter))
                        {
                            Console.WriteLine("Tu a déjà mis cette lettre auparavant, donne en une autre");
                            return GetLetter(RightLetter, DecomWord, WrongLetter);
                        }
                        else
                        {
                            return Letter;
                        }
                        return GetLetter(RightLetter, DecomWord, WrongLetter);
                    }
                    else
                    {
                        Console.WriteLine("Tu ne peut pas mettre de caractères spéciaux ou de nombres !");
                        Console.WriteLine("");
                        Console.Write(": ");
                        return GetLetter(RightLetter, DecomWord, WrongLetter);
                    }
                    return GetLetter(RightLetter, DecomWord, WrongLetter);
                }
                else
                {
                    Console.WriteLine("Tu ne peut pas mettre plus que 1 caractère !");
                    Console.WriteLine("");
                    Console.Write(": ");
                    return GetLetter(RightLetter, DecomWord, WrongLetter);
                }
                return GetLetter(RightLetter, DecomWord, WrongLetter);
            }
            catch
            {
                return GetLetter(RightLetter, DecomWord, WrongLetter);
            }
        }
    }
}
